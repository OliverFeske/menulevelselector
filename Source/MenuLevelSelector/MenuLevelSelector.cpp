// Copyright Epic Games, Inc. All Rights Reserved.

#include "MenuLevelSelector.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MenuLevelSelector, "MenuLevelSelector" );

DEFINE_LOG_CATEGORY(LogMenuLevelSelector)
 