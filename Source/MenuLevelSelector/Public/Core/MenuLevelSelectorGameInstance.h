// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "UI/menuInterface.h"

#include "MenuLevelSelectorGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MENULEVELSELECTOR_API UMenuLevelSelectorGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

#pragma region UI
// ------------------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintCallable)
		void LoadInGameMenuWidget(TArray<FName> ButtonsThatTriggerClose);
	UFUNCTION(BlueprintCallable)
		void LoadLoadingScreenWidget();
	
private:
	UPROPERTY(EditAnywhere, Category = "UI Classes")
		TSubclassOf<class UMenuWidget> InGameMenuClass;
	UPROPERTY(EditAnywhere, Category = "UI Classes")
		TSubclassOf<class UUserWidget> LoadingScreenClass;

	UPROPERTY()
		class UInGameMenuWidget* InGameMenu;
	UPROPERTY()
		class ULoadingScreenWidget* LoadingScreen;
#pragma endregion
#pragma region Interface
	virtual void LoadSelectedLevel(FString& LevelName) override;
#pragma endregion
};
