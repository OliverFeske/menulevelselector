// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SelectableLevelDataAsset.generated.h"


USTRUCT(BlueprintType)
struct FLevelInfo
{
	GENERATED_BODY()

public:
	// Check if all the entries of the LevelInfo are valid.
	bool CheckIsValid(uint16 i)
	{
		Index = i;

		if (CheckLevelName() && CheckLevelDescription() && CheckLevelImage() && CheckInternalName())
			return true;
		else
			return false;
	}

	bool CheckLevelName()
	{
		if (DisplayName != "")
			return true;
		else
		{
			UE_LOG(LogTemp, Error, TEXT("The NAME of the level at Index[ %i ] is empty"), Index);
			return false;
		}
	}

	bool CheckLevelDescription()
	{
		if (Description != "")
			return true;
		else
		{
			UE_LOG(LogTemp, Error, TEXT("The DESCRIPTION of the level at Index[ %i ] is empty"), Index);
			return false;
		}
	}

	bool CheckLevelImage()
	{
		if (Image != nullptr)
			return true;
		else
		{
			UE_LOG(LogTemp, Error, TEXT("The IMAGE of the level at Index[ %i ] is invalid"), Index);
			return false;
		}
	}

	bool CheckInternalName()
	{
		if (InternalName != "")
			return true;
		else
		{
			UE_LOG(LogTemp, Error, TEXT("The INTERNALNAME of the level at Index[ %i ] is empty"), Index);
			return false;
		}
	}

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Visible Properties")
		FString DisplayName {""};
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Visible Properties")
		FString Description {""};
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Visible Properties")
		UTexture2D* Image;
	UPROPERTY(EditAnywhere, BlueprintReadOnly,Category = "Hidden Properties")
		FString InternalName {""};

private:
	UPROPERTY()
		uint16 Index{ 0 };
};

UCLASS(BlueprintType)
class MENULEVELSELECTOR_API USelectableLevelDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FLevelInfo> Levels;
};