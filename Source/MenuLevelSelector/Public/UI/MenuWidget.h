// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

UENUM(BlueprintType)
enum PromptState
{
	Confirmed,
	Declined
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPromptSelected, PromptState, D_OnPromptSelected);

UCLASS()
class MENULEVELSELECTOR_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

public:
	virtual void Setup(class IMenuInterface* IMenu, TArray<FName>& ButtonsThatTriggerClose);
	UFUNCTION(BlueprintCallable)
		virtual void CloseMenu();
	UFUNCTION(BlueprintCallable)
		void CreatePrompt(TSubclassOf<class UPromptWidget> PromptWidgetClass, FString PromptMessageString,
			FString ConfirmString = "Confirm", FString DeclineString = "Decline");

	UPROPERTY(BlueprintAssignable)
		FPromptSelected D_OnPromptSelected;

protected:
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
		class UButton* Button_CloseMenu;

	IMenuInterface* MenuInterface;

private:
	UPROPERTY()
		class UPromptWidget* PromptWidget;

	FOnInputAction Action;
};
