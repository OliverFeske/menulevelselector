// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UI/MenuWidget.h"
#include "UI/SelectableLevelDataAsset.h"

#include "InGameMenuWidget.generated.h"

UCLASS()
class MENULEVELSELECTOR_API UInGameMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

public:
	UFUNCTION(BlueprintCallable)
		void CheckForValidLevels();
	UFUNCTION(BlueprintCallable)
		void LoadSelectedLevel();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		USelectableLevelDataAsset* LevelDataAsset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString CurrentSelectedInternalName {""};
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString CurrentLevelName {""};

protected:
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
		class UButton* Button_QuitGame;
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
		class UButton* Button_LoadSelectedLevel;
};
