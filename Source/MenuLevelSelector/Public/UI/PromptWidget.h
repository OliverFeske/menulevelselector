// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PromptWidget.generated.h"

UCLASS()
class MENULEVELSELECTOR_API UPromptWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void Setup(class UMenuWidget* InParent, FString& PromptMessageString, FString& ConfirmString, FString& DeclineString);
	UFUNCTION(BlueprintCallable)
		void Confirm();
	UFUNCTION(BlueprintCallable)
		void Decline();

protected:
	void RemovePrompt();

	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
		class UButton* Button_Confirm;
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
		class UTextBlock* TextBlock_PromptMessage;
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
		class UTextBlock* TextBlock_ConfirmString;

	UPROPERTY(BlueprintReadWrite)
		FString PromptMessage;
	UPROPERTY(BlueprintReadWrite)
		FString ButtonConfirmString;
	UPROPERTY(BlueprintReadWrite)
		FString ButtonDeclineString;

	UPROPERTY(BlueprintReadOnly)
		class UMenuWidget* Parent;
};