// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/MenuLevelSelectorGameInstance.h"

#include "UI/InGameMenuWidget.h"
#include "UI/LoadingScreenWidget.h"

void UMenuLevelSelectorGameInstance::LoadInGameMenuWidget(TArray<FName> ButtonsThatTriggerClose)
{
	if (!ensure(InGameMenuClass != nullptr)) return;

	InGameMenu = CreateWidget<UInGameMenuWidget>(this, InGameMenuClass);
	if (!ensure(InGameMenu != nullptr)) return;

	InGameMenu->Setup(this, ButtonsThatTriggerClose);
}

void UMenuLevelSelectorGameInstance::LoadLoadingScreenWidget()
{
	if (!ensure(LoadingScreenClass != nullptr)) return;

	LoadingScreen = CreateWidget<ULoadingScreenWidget>(this, LoadingScreenClass);
	if (!ensure(LoadingScreen != nullptr)) return;

	LoadingScreen->bIsFocusable = true;
	LoadingScreen->AddToViewport();
}

void UMenuLevelSelectorGameInstance::LoadSelectedLevel(FString& LevelName)
{
	LoadLoadingScreenWidget();

	FString LevelURL = "/Game/Maps/" + LevelName;
	if (!GetWorld()->ServerTravel(LevelURL))
		if (LoadingScreen != nullptr)
			LoadingScreen->Remove();
}