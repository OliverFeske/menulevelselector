// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/LoadingScreenWidget.h"

void ULoadingScreenWidget::Remove()
{
	this->bIsFocusable = false;
	this->RemoveFromViewport();
}