// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MenuWidget.h"
#include "UI/MenuInterface.h"
#include "UI/PromptWidget.h"

#include "Components/Button.h"


bool UMenuWidget::Initialize()
{
	if (!Super::Initialize()) return false;

	if (!ensure(Button_CloseMenu != nullptr)) return false;
	Button_CloseMenu->OnClicked.AddDynamic(this, &UMenuWidget::CloseMenu);

	return true;
}

void UMenuWidget::Setup(IMenuInterface* IMenu, TArray<FName>& ButtonsThatTriggerClose)
{
	MenuInterface = IMenu;

	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (!ensure(PC != nullptr)) return;

	FInputModeGameAndUI InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());

	PC->SetInputMode(InputModeData);

	this->bStopAction = true;
	Action.BindUFunction(this, "CloseMenu");

	for (auto ButtonName : ButtonsThatTriggerClose)
	{
		ListenForInputAction(ButtonName, EInputEvent::IE_Pressed, true, Action);
	}

	this->bIsFocusable = true;
	this->AddToViewport();
}

void UMenuWidget::CloseMenu()
{
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (!ensure(PC != nullptr)) return;

	Action.Clear();

	this->bIsFocusable = false;
	this->RemoveFromViewport();
}

void UMenuWidget::CreatePrompt(TSubclassOf<UPromptWidget> PromptWidgetClass, FString PromptMessageString, FString ConfirmString, FString DeclineString)
{
	if (!ensure(PromptWidgetClass != nullptr)) return;

	PromptWidget = CreateWidget<UPromptWidget>(this, PromptWidgetClass);
	if (!ensure(PromptWidget != nullptr)) return;

	PromptWidget->Setup(this, PromptMessageString, ConfirmString, DeclineString);
}