// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/InGameMenuWidget.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Components/Button.h"

#include "UI/MenuInterface.h"

bool UInGameMenuWidget::Initialize()
{
	if (!Super::Initialize()) return false;

	CheckForValidLevels();

	return true;
}

void UInGameMenuWidget::CheckForValidLevels()
{
	for (size_t i = 0; i < LevelDataAsset->Levels.Num(); i++)
	{
		if (!LevelDataAsset->Levels[i].CheckIsValid(i))
		{
			LevelDataAsset->Levels.RemoveAt(i);
		}
	}
}

void UInGameMenuWidget::LoadSelectedLevel()
{
	MenuInterface->LoadSelectedLevel(CurrentSelectedInternalName);
}