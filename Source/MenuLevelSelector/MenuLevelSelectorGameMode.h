// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MenuLevelSelectorGameMode.generated.h"

UCLASS(minimalapi)
class AMenuLevelSelectorGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMenuLevelSelectorGameMode();

	virtual void BeginPlay() override;

protected:
	// This is not neccessary in a game
	// I just put it here because this is the GameMode in every level and we want to see these instructions everywhere
	UPROPERTY(EditAnywhere, Category = "Placeholder UI")
		TSubclassOf<class UUserWidget> InputInstructionsWidgetClass;

	UPROPERTY()
		class UUserWidget* InputInstructionsWidget;
	UPROPERTY(BlueprintReadWrite)
		TArray<FName> BTTC_InGameMenu;
};



